#!/bin/sh

# Generate local configuration files from the cluster configuration file
# and the local hostname.  Assumes that the configuration has already been
# validated and tested.

[ `whoami` = root ] || exec sudo $0 $*

. /etc/appliance/appliance.subr

HOSTNAME=$1
CLUSTER_CONF=$2

echo "Reading cluster configuration from $CLUSTER_CONF"
. $CLUSTER_CONF

if [ "$HOSTNAME" = "$appliance_1_hostname" ]; then
  WHICH_APPLIANCE=appliance_1
  SIBLING_APPLIANCE=appliance_2
elif [ "$HOSTNAME" = "$appliance_2_hostname" ]; then
  WHICH_APPLIANCE=appliance_2
  SIBLING_APPLIANCE=appliance_1
else
  echo "ERROR: Can't figure out who I'm supposed to be..."
  exit 255
fi

eval MY_PORT1_IP=$"$WHICH_APPLIANCE"_port1_ip
eval MY_PORT2_IP=$"$WHICH_APPLIANCE"_port2_ip

eval SIBLING_HOST=$"$SIBLING_APPLIANCE"_hostname
eval SIBLING_PORT1_IP=$"$SIBLING_APPLIANCE"_port1_ip

echo "Writing $RC_CONF_FILE ..."
echo "sshd_enable=\"YES\"" > $RC_CONF_FILE
echo "ntpd_enable=\"YES\"" >> $RC_CONF_FILE
echo "ntpd_sync_on_start=\"YES\"" >> $RC_CONF_FILE
echo "hostname=\"$HOSTNAME\"" >> $RC_CONF_FILE

if [ "$net1_dhcp" -eq "1" ]; then
  echo "ifconfig_$PORT1_IF=\"DHCP\"" >> $RC_CONF_FILE
else
  if [ -z "$net1_netmask" ]; then
    echo "Must specify netmask if not using DHCP"
    exit 255
  fi
  if [ -z "$nameserver" -a -z "$MY_PORT1_IP" ]; then
	echo "Must specify Port 1 IP if not using DHCP and DNS is not available"
    exit 255
  fi
  if [ ! -z "$nameserver" ]; then
	get_ips_for_hostname $HOSTNAME $nameserver
    if [ -z "$HOSTNAME_IPS" ]; then
	  echo "Couldn't resolve $HOSTNAME"
	  exit 255
	else
 	  MY_PORT1_IP=$HOSTNAME_IPS
      for ip in $HOSTNAME_IPS
      do
		MY_PORT1_IP=$ip
        break
      done
    fi
  fi
  echo "ifconfig_$PORT1_IF=\"inet $MY_PORT1_IP netmask $net1_netmask\"" >> $RC_CONF_FILE
  if [ -z "$net1_gateway" ]; then
    echo "WARNING: no default route.  Addresses not on the LAN will be inaccessible"
  else
    echo "defaultrouter=\"$net1_gateway\"" >> $RC_CONF_FILE
  fi
fi

if [ "$net2_enabled" -eq "1" ]; then
  if [ "$net2_dhcp" -eq "1" ]; then
    echo "ifconfig_$PORT2_IF=\"DHCP\"" >> $RC_CONF_FILE
  else
    if [ -z "$MY_PORT2_IP" -o -z "$net2_netmask" ]; then
      echo "ERROR: Port 2 is enabled, but IP and netmask are not set"
      exit 255
    fi
    echo "ifconfig_$PORT2_IF=\"inet $MY_PORT2_IP netmask $net2_netmask\"" >> $RC_CONF_FILE
  fi
fi

echo "shadoww_enable=\"YES\"" >> $RC_CONF_FILE

echo "Writing $NTP_CONF_FILE ..."
generate_ntp_conf "$timeserver"

if [ -z "$nameserver" ]; then
  echo "Writing empty $RESOLV_CONF_FILE ..."
  generate_resolv_conf

  if [ -z "$es_ip" ]; then
    echo "ERROR: ES IP must be set, since DNS is not available"
    exit 255
  fi
  echo "Writing complete $HOSTS_FILE ..."
  generate_hosts "$HOSTNAME" "$es_ip" "$es_hostname" "$SIBLING_PORT1_IP" "$SIBLING_HOST"
else
  echo "Writing $RESOLV_CONF_FILE ..."
  generate_resolv_conf "$nameserver"

  echo "Writing local $HOSTS_FILE ..."
  generate_hosts "$HOSTNAME"
fi

cp $CLUSTER_CONF $CLUSTER_CONF_FILE
echo "Saving generated configuration files ..."
save_cfg

echo "Generating SHADOWW cluster profiles ..."
generate_shadoww_config $es_hostname $SIBLING_HOST

echo "Activating \"clustered\" profile and saving SHADOWW configuration .."
activate_clustered_profile
save_shadoww_config

echo "Configuration complete!"

echo "Rebooting..."
(sleep 1; reboot > /dev/null) &

exit 0

