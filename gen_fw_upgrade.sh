#!/bin/sh

code_img=$1
pkg_name=$2
build_tag=$3
build_date=$4

private_key=id_rsa
base_manifest=base.manifest

ERROR=0

if test ! -r $code_img ; then
    echo "Code partition image file must exist"
    ERROR=1
fi

if test -e $pkg_name ; then
    echo "Output package file name $pkg_name should not exist"
    ERROR=1
fi

if test $ERROR -eq 1 ; then
    echo "Usage: $0 <code partition image name> <output package name>"
    exit
fi

ver=`cat Files/etc/appliance/ver`

echo "Generating a firmware update package to version $ver $build_tag $build_date from $code_img"
echo

workdir=`env TMPDIR=. mktemp -d -t fwupgrade`

echo "Building package in $workdir"

cat > $workdir/upgrade <<"UPGRADE_END"
#!/bin/sh

stage_dir=$1
update_cmd=/root/update

# Default upgrade script
# 'firmware' contains a compressed disk partition

xzcat $stage_dir/firmware | $update_cmd
UPGRADE_END

chmod +x $workdir/upgrade

echo "Compressing code image..."
pxz -c -9 -T4 $code_img > $workdir/firmware

echo "Building manifest..."
mf=$workdir/.manifest
cp $base_manifest $mf

echo "version=$ver" >> $mf
echo "build_tag=$build_tag" >> $mf
echo "build_date=$build_date" >> $mf

files=`ls $workdir/*`
for file in $files
do
    hash=`sha256 -q $file`
    fbase=`basename $file`
    echo "${fbase}_sha256=$hash" >> $mf
done

echo "Signing manifest..."
sha256 -q $mf | openssl pkeyutl -sign -inkey $private_key > $workdir/signature

mv $mf $workdir/manifest

echo "Building package..."
tar -cf $pkg_name -C $workdir .

echo "Cleaning up..."
rm -Rf $workdir

echo
echo "All done!"

