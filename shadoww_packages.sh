#!/bin/sh

rm -Rf Pkg All
pkg fetch -y -U -o . -d pkg
pkg fetch -y -U -o . -d openjdk8
pkg fetch -y -U -o . -d sudo
mv All Pkg
