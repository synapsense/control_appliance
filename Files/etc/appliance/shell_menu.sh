#!/bin/sh

. /etc/appliance/appliance.subr

main() {
  echo
  uptime
  vmstat
  ifconfig $PORT1_IF
  echo "The S.H.A.D.O.W.W. $APPLIANCE_VERSION $BUILD_TAG $BUILD_DATE is watching..."
  echo ""
  echo "1) Change IP"
  echo "2) Change password"
  echo "3) top"
  echo "4) Revert to factory defaults"
  echo "5) reboot"

  choice=0
  read -t 60 -p "Option: " choice
  case $choice in
    1)
      change_ip
      ;;
    2)
      change_password
      ;;
    3)
      top
      ;;
    4)
      do_factory_defaults
      ;;
    5)
      reboot
      ;;
    "watch the watcher")
      tcsh
      ;;
    *)
      return
  esac
}

change_ip() {
  if [ -f $RC_CONF_FILE ]; then
    echo ""
    echo "                  !!!WARNING!!!"
    echo ""
    echo "Changing the IP address will stop all services and clear all"
    echo "other configuration.  The appliance configuration tool will"
    echo "have to be re-run from the Environment Server."
    echo ""
    read -p "Press \"Y\" to continue: " choice
    if [ "$choice" != "Y" ]; then
      return
    fi
  fi

  read_ip_address
  if [ "$IP" != "DHCP" ]; then
    read_netmask
    read_default_gateway
  fi

  echo "Stopping services ..."
  $SHADOWW_RC onestop
  /etc/rc.d/ntpd onestop
  /etc/rc.d/sshd onestop

  echo "Resetting appliance configuration ..."
  if [ -f $RC_CONF_FILE ]; then
    rm $RC_CONF_FILE
  fi
  generate_ntp_conf
  generate_resolv_conf
  generate_hosts

  echo "Disabling Port2 ($PORT2_IF) ..."
  ifconfig $PORT2_IF down

  echo "Setting Port1 ($PORT1_IF) address ..."
  if [ "$IP" = "DHCP" ]; then
    dhclient $PORT1_IF
    echo "ifconfig_$PORT1_IF=\"DHCP\"" >> $RC_CONF_FILE
  else
    ifconfig $PORT1_IF inet $IP netmask $NETMASK
    echo "ifconfig_$PORT1_IF=\"inet $IP netmask $NETMASK\"" >> $RC_CONF_FILE
    if [ ! -z "$DEFAULTROUTER" ]
    then
      route delete default
      route add default $DEFAULTROUTER
      echo "defaultrouter=\"$DEFAULTROUTER\"" >> $RC_CONF_FILE
    fi
  fi

  echo "Starting sshd ..."
  echo "sshd_enable=\"YES\"" >> $RC_CONF_FILE
  /etc/rc.d/sshd start

  echo "Saving configuration changes ..."
  save_cfg
  echo "Saving SSH keys ..."
  save_ssh_keys

  echo ""
  echo "Done!  ready for remote cluster configuration"
}

read_ip_address() {
  read -p "New IP address (1.2.3.4) or \"DHCP\" for auto-assigned: " new_ip
  if echo $new_ip | grep -Eq $IP_REGEX
  then
    IP=$new_ip
  elif [ "$new_ip" = "DHCP" ]
  then
    IP=$new_ip
  else
    echo "\"$new_ip\" Bad format, please try again"
    read_ip_address
  fi
}

read_netmask() {
  read -p "Netmask (255.255.255.0): " new_netmask
  if [ -z "$new_netmask" ]
  then
    NETMASK="255.255.255.0"
  elif echo $new_netmask | grep -Eq $IP_REGEX
  then
    NETMASK=$new_netmask
  else
    echo "\"$new_netmask\" Bad format, please try again"
    read_netmask
  fi
}

read_default_gateway() {
  read -p "Default Gateway (<Enter> for none): " new_router
  if [ -z "$new_router" ]
  then
    DEFAULTROUTER=""
  elif echo $new_router | grep -Eq $IP_REGEX
  then
    DEFAULTROUTER=$new_router
  else
    echo "\"$new_router\" Bad format, please try again"
    read_default_gateway
  fi
}

change_password() {
  passwd shadoww
  save_passwd
}

do_factory_defaults() {
  clear_cfg
  reboot
}

while true :
do
  main
done

