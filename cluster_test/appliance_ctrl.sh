#!/bin/sh

username=shadoww
script_path=/etc/appliance
cluster_cfg=/var/tmp/cluster_config.rc
img_path=/logs/firmware.update

export SSHPASS=admin
export SSH_ASKPASS=./apass

ssh_opt="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
ssh="sshpass -e ssh $ssh_opt -l $username"
scp="sshpass -e scp $ssh_opt"

ping="ping -n -o -q"

configure() {
  cfg=$1

  if test -z "$cfg" -o \( ! -r $cfg \)
  then
    usage
    exit 1
  fi

  echo "Using config '$cfg'"
  . $cfg

  echo ""

  echo "Copy $cfg to $appliance_1_hostname"
  $scp $cfg $username@$appliance_1_port1_ip:$cluster_cfg
  echo "Copy $cfg to $appliance_2_hostname"
  $scp $cfg $username@$appliance_2_port1_ip:$cluster_cfg

  echo ""

  echo "Validate config on $appliance_1_hostname"
  $ssh $appliance_1_port1_ip $script_path/validate_config.sh $appliance_1_hostname $cluster_cfg
  echo "Validate config on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/validate_config.sh $appliance_2_hostname $cluster_cfg

  echo ""

  echo "Test config on $appliance_1_hostname"
  $ssh $appliance_1_port1_ip $script_path/test_config.sh $appliance_1_hostname $cluster_cfg
  echo "Test config on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/test_config.sh $appliance_2_hostname $cluster_cfg

  echo ""

  echo "Generate config on $appliance_1_hostname"
  $ssh $appliance_1_port1_ip $script_path/gen_config.sh $appliance_1_hostname $cluster_cfg
  echo "Generate config on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/gen_config.sh $appliance_2_hostname $cluster_cfg
}

fast_upgrade() {
  cfg=$1
  img=$2

  if test -z "$cfg" -o -z "$img" -o ! \( -r "$cfg" -a -r "$img" \)
  then
    usage
    exit 1
  fi

  echo "Using config '$cfg'"
  . $cfg

  echo "Copying firmware images ..."
  $scp $img $username@$appliance_1_port1_ip:$img_path &
  $scp $img $username@$appliance_2_port1_ip:$img_path &
  wait

  echo "Applying upgrade ..."
  $ssh $appliance_1_port1_ip $script_path/apply_upgrade.sh $img_path &
  $ssh $appliance_2_port1_ip $script_path/apply_upgrade.sh $img_path &
  wait

  echo "Done!"
}

upgrade() {
  cfg=$1
  img=$2

  if test -z "$cfg" -o -z "$img" -o ! \( -r "$cfg" -a -r "$img" \)
  then
    usage
    exit 1
  fi

  echo "Using config '$cfg'"
  . $cfg

  echo ""

  echo "Copy firmware image to $appliance_1_hostname"
  $scp $img $username@$appliance_1_port1_ip:$img_path
  echo "Copy firmware image to $appliance_2_hostname"
  $scp $img $username@$appliance_2_port1_ip:$img_path

  echo ""

  echo "Apply upgrade on $appliance_1_port1_ip"
  $ssh $appliance_1_port1_ip $script_path/apply_upgrade.sh $img_path

  wait_sec=30
  echo "Waiting $wait_sec sec for appliance to reboot..."
  sleep $wait_sec

  echo "Waiting for $appliance_1_hostname to come back online and stable"
  # ping returns on first response
  $ping $appliance_1_port1_ip
  echo "Got ping, waiting for services to start..."
  sleep 10
  $ssh $appliance_1_port1_ip $script_path/shadoww_control.sh cluster_stable

  echo "Apply upgrade on $appliance_2_port1_ip"
  $ssh $appliance_2_port1_ip $script_path/apply_upgrade.sh $img_path
}

full_upgrade() {
  cfg=$1
  img=$2

  if test -z "$cfg" -o -z "$img" -o ! \( -r "$cfg" -a -r "$img" \)
  then
    usage
    exit 1
  fi

  echo "Using config '$cfg'"
  . $cfg

  echo ""

  echo "Copy firmware image to $appliance_1_hostname"
  $scp $img $username@$appliance_1_port1_ip:$img_path
  echo "Copy firmware image to $appliance_2_hostname"
  $scp $img $username@$appliance_2_port1_ip:$img_path

  echo ""

  echo "Pausing ES sync..."
  $ssh $appliance_1_port1_ip $script_path/shadoww_control.sh pause_es_sync

  echo "Press any key when ES upgrade is finished (will timeout after 5 min)..."
  read -t 300 n

  echo "Tell $appliance_1_hostname it's sister will be offline for a while"
  $ssh $appliance_1_port1_ip $script_path/shadoww_control.sh pause_clustering $appliance_2_hostname

  echo "Stopping SHADOWW service on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh stop

  echo "Setting 'isolated' cluster profile on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh profile isolated

  echo "Apply upgrade on $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/apply_upgrade.sh $img_path

  wait_sec=30
  echo "Waiting $wait_sec sec for appliance to reboot..."
  sleep $wait_sec

  echo "Waiting for $appliance_2_hostname to come back online and stable"
  # ping returns on first response
  $ping $appliance_2_port1_ip
  echo "Got ping, waiting for services to start..."
  sleep 10
  # TODO: should not continue if this fails
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh cluster_stable

  echo "Stopping SHADOWW service on $appliance_1_hostname"
  $ssh $appliance_1_port1_ip $script_path/shadoww_control.sh stop

  echo "Spinning up $appliance_2_hostname"
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh profile clustered
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh resume_clustering $appliance_1_hostname
  $ssh $appliance_2_port1_ip $script_path/shadoww_control.sh resume_es_sync

  echo "Apply upgrade on $appliance_1_hostname"
  $ssh $appliance_1_port1_ip $script_path/apply_upgrade.sh $img_path
}

usage() {
    echo "Usage:"
    echo "    $0 configure <cluster_config>"
    echo "    $0 upgrade <cluster_config> <firmware image>"
    echo "    $0 fast_upgrade <cluster_config> <firmware image>"
    echo "    $0 full_upgrade <cluster_config> <firmware image>"
    echo " Note: full_upgrade requires user interaction"
}

command=$1
shift

case $command in
  configure)
    configure $*
    ;;
  upgrade)
    upgrade $*
    ;;
  full_upgrade)
    full_upgrade $*
    ;;
  fast_upgrade)
    fast_upgrade $*
    ;;
  * )
    usage
    exit 1
    ;;
esac


