#!/bin/sh

# test_config.sh - test as much of the cluster configuration as possible.
# ie, that the DNS server can resolve names

# ##### WARNING #####
# ##### WARNING #####
# ##### WARNING #####
# assumes that services like control, NTP and others have been stopped.
# assumes that the Port1 IP address is already set (correctly)
# the Port2 IP address will be reset

# TODO: change_ip usecase.

[ `whoami` = root ] || exec sudo $0 $*

. /etc/appliance/appliance.subr

SNTP="sntp -S"

HOSTNAME=$1
CLUSTER_CONF=$2

BEAM_RUNNING=`ps ax | grep beam.smp | grep -v grep | wc -l | cut -w -f 2`
if [ "$BEAM_RUNNING" -gt "0" ]; then
  echo "WARNING: 'SHADOWW' seems to be running. Stopping it first..."
  $SHADOWW_RC onestop
fi

NTPD_RUNNING=`ps ax | grep ntpd | grep -v grep | wc -l | cut -w -f 2`
if [ "$NTPD_RUNNING" -gt "0" ]; then
  echo "WARNING: 'ntpd' seems to be running. Stopping it first..."
  /etc/rc.d/ntpd onestop
fi

EXIT=0

. $CLUSTER_CONF

if [ "$HOSTNAME" = "$appliance_1_hostname" ]; then
  WHICH_APPLIANCE=appliance_1
  SIBLING_APPLIANCE=appliance_2
elif [ "$HOSTNAME" = "$appliance_2_hostname" ]; then
  WHICH_APPLIANCE=appliance_2
  SIBLING_APPLIANCE=appliance_1
else
  echo "ERROR: Can't figure out who I'm supposed to be..."
  EXIT=255
fi

eval MY_PORT1_IP=$"$WHICH_APPLIANCE"_port1_ip
eval MY_PORT2_IP=$"$WHICH_APPLIANCE"_port2_ip

eval SIBLING_HOST=$"$SIBLING_APPLIANCE"_hostname
eval SIBLING_PORT1_IP=$"$SIBLING_APPLIANCE"_port1_ip
eval SIBLING_PORT2_IP=$"$SIBLING_APPLIANCE"_port2_ip

echo "Resetting appliance configuration ..."

if [ -f $RC_CONF_FILE ]; then
  rm $RC_CONF_FILE
fi
generate_ntp_conf
generate_resolv_conf
generate_hosts

echo "Disabling Port2 ($PORT2_IF) ..."
ifconfig $PORT2_IF down

echo "Reconfiguring Port1 ($PORT1_IF) routing ..."
if [ ! -z "$DEFAULTROUTER" ]
then
  netstat -rn
  route delete default
  route add default $DEFAULTROUTER
  netstat -rn
fi

if [ -z "$nameserver" ]; then

  echo "No nameserver.  Testing addresses ..."

  if [ -z "$SIBLING_PORT1_IP" ]; then
    echo "ERROR: Sibling IP not set"
    EXIT=255
  else
    if $PING $SIBLING_PORT1_IP; then
      echo "Sibling address $SIBLING_PORT1_IP looks good"
    else
      echo "ERROR: can't ping sibling address: $SIBLING_PORT1_IP"
      EXIT=255
    fi
  fi
  if [ -z "$es_ip" ]; then
    echo "ERROR: ES IP not set"
    EXIT=255
  else
    if $PING $es_ip; then
      echo "ES address $es_ip looks good"
    else
      echo "ERROR: can't ping ES address: $es_ip"
      EXIT=255
    fi
  fi

  echo "Checking connection to ControlTransport..."
  `nc -z -w 10 $es_ip 9382`
  if [ "$?" = "0" ]; then
    echo "Successfully connected to control transport on $es_ip"
  else
    echo "ERROR: can't connect to control transport on $es_ip"
    EXIT=255
  fi

else
  echo "Checking DNS and connectivity ..."

  default_network=`$IPCALC $DEFAULTROUTER | sed -n '/Network:/ s#^Network:[ \t]*\([0-9.]*\)/[0-9]*[ \t]*[01. ]*#\1#p'`

  get_pingable_ip $SIBLING_HOST $nameserver $default_network
  SIBLING_IP="$IP_ADDRESS" 
  if [ -z "$SIBLING_IP" ]; then
    echo "ERROR: Unable to contact sibling"
    EXIT=255
  else
    echo "Sibling IP: $SIBLING_IP"
  fi

  get_pingable_ip $es_hostname $nameserver $default_network
  ES_IP="$IP_ADDRESS" 
  if [ -z "$ES_IP" ]; then
    echo "ERROR: Unable to contact ES"
    EXIT=255
  else
    echo "ES IP: $ES_IP"
  fi

  echo "Checking connection to ControlTransport..."
  `nc -z -w 10 $es_hostname 9382`
  if [ "$?" = "0" ]; then
    echo "Successfully connected to control transport on $es_hostname"
  else
    echo "ERROR: can't connect to control transport on $es_hostname"
    EXIT=255
  fi

fi

echo "Checking NTP ..."
VALID_NTP=""
if [ -z "$timeserver" ]; then
else
  for ntpserver in $(echo $timeserver | tr "," " ")
  do
    ntpIP=""
    if echo $ntpserver | grep -Eq $IP_REGEX
    then
      # Have a valid IP address
	  ntpIP=$ntpserver
    else
      if [ -z $nameserver ]; then
        echo "ERROR: Time server must be IP address if no DNS"
	  else
        get_pingable_ip $ntpserver $nameserver
        ntpIP=$IP_ADDRESS
      fi
    fi

    if [ -z $ntpIP ]; then
	  echo "ERROR: Bad time server, '$ntpIP'"
    else
      if $PING $ntpIP; then
        echo "Time server contacted"
      else
  	    echo "ERROR: Can't ping $ntpIP"
      fi

      if $SNTP $ntpIP; then
        DATE=`date`
        echo "Current time: $DATE"
		VALID_NTP=$ntpIP
      else
        echo "Setting time from $ntpIP failed"
      fi
    fi
  done

  if [ -z $VALID_NTP ]; then
    echo "ERROR: Unable to set time from any of the specified time servers: '$timeserver'"
    EXIT=255
  else
    echo "NTP looks good"
  fi
fi


if [ -z "$SIBLING_PORT2_IP" ]; then
  echo "Port2 is disabled.  Skipping ..."
else
  echo "Port2 is in use.  Testing connectivity ..."
  if $PING $SIBLING_PORT1_IP; then
    echo "Port2 settings look good"
  else
    echo "ERROR: Contacting sibling on Port2 failed!"
    EXIT=255
  fi
fi

if [ $EXIT -eq 0 ]; then
  echo "Configuration test SUCCEEDED"
else
  echo "ERROR: Configuration test FAILED"
fi

exit $EXIT

