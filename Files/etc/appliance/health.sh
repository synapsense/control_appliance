#!/bin/sh

# health.sh - print diagnostic info to stdout
# it is assumed that the client will save the output to a file
# since there may be a lot of it

e() {
  echo "----------------------------------------"
  echo ": $@"
  echo "----------------------------------------"
  "$@"
}

e date
e uname -a
e cat /etc/appliance/ver
echo
e cat /etc/appliance/build.date
e cat /etc/appliance/build.tag
e cat /shadoww/www/build_stamp
e cat /etc/cluster.conf
e cat /etc/shadoww_cluster.config
e cat /etc/rc.conf
e vmstat -h -P -c 2
e ps axwwuvrd
e df -hi
e ifconfig
e netstat -nar
e netstat -na
e dmesg
e tail -n 100 /var/log/messages
e ls -la /logs
e cat /logs/cluster_config.overrides
echo
e tail -n 100 /logs/crash.log
e tail -n 100 /logs/error.log
e tail -n 100 /logs/info.log
echo "----------------------------------------"
echo "All Done!"
