#!/bin/sh

[ `whoami` = root ] || exec sudo $0 $*

update_pkg=$1
extract_dir=`env TMPDIR=/logs mktemp -d -t fwupgrade`
pubkey=/etc/appliance/id_rsa.pub.pkcs8
EXIT=0

. /etc/appliance/appliance.subr

echo "Stopping SHADOWW"
$SHADOWW_RC onestop

rm -Rf $extract_dir
mkdir -p $extract_dir

echo "Extracting update..."
if tar -xf $update_pkg -C $extract_dir
then
  if [ ! -f $extract_dir/manifest -o ! -f $extract_dir/signature ]
  then
    echo "Upgrade package is missing files!"
    echo "Cleaning up..."
    rm -Rf $extract_dir $update_pkg
    exit 6
  fi
else
  echo "Couldn't extract firmware upgrade package '$update_pkg'!"
  echo "Cleaning up..."
  rm -Rf $extract_dir $update_pkg
  exit 5
fi

trap "rm -Rf $extract_dir $update_pkg;reboot" EXIT

echo "Extract complete, verifying signature..."
# NOTE: exit code of OpenSSL pkeyutl -verify broken in openssl 1.0.1j, forcing
# this workaround.  A fix is present in upstream.  When it's merged to FreeBSD,
# this workaround should be removed.
# Details:
# http://rt.openssl.org/Ticket/Display.html?user=guest&pass=guest&id=2618
v_result=`sha256 -q $extract_dir/manifest | openssl pkeyutl -verify -pubin -inkey $pubkey -sigfile $extract_dir/signature`
if [ "$v_result" = "Signature Verified Successfully" ]
then
  . $extract_dir/manifest
  for file in `ls $extract_dir`
  do
    if [ $file = "manifest" -o $file = "signature" ]
    then
      continue
    fi
    hash=`sha256 -q $extract_dir/$file`
    eval stored=\$${file}_sha256
    if [ "$hash" != "$stored" ]
    then
      echo "Checksum verification failed for '$file'"
      EXIT=1
    fi
  done
  if [ $EXIT -eq 0 ]
  then
    echo "Signature verification successful."
    echo "Extracting disk image..."
    echo "CURRENT: $BUILD_TAG $BUILD_DATE"
    echo "NEW: $build_tag $build_date"
    if [ -f $extract_dir/upgrade -a -x $extract_dir/upgrade ]
    then
      $extract_dir/upgrade $extract_dir
    else
      echo "Can't execute upgrade!"
      EXIT=4
    fi
  else
    echo "Aborting update."
  fi
elif [ "$v_result" = "Signature Verification Failure" ]
then
  echo "Signature verification failed!"
  EXIT=2
else
  echo "Signature could not be verified!"
  EXIT=3
fi

echo "Cleaning up..."
rm -Rf $extract_dir $update_pkg

trap EXIT

echo "Rebooting..."
(sleep 1; reboot > /dev/null) &

exit $EXIT

