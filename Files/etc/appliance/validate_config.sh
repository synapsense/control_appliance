#!/bin/sh

# validate_config.sh - validate the structure of the cluster.conf file
# ie, that the IP address is set if DHCP is not used,
# that the net2_netmask is set if net2 is enabled, etc.
# validating the contents of the configuration is up to another,
# more complete script: test_config.sh

HOSTNAME=$1
CLUSTER_CONF=$2

. $CLUSTER_CONF

if [ "$HOSTNAME" = "$appliance_1_hostname" ]; then
  WHICH_APPLIANCE=appliance_1
  SIBLING_APPLIANCE=appliance_2
elif [ "$HOSTNAME" = "$appliance_2_hostname" ]; then
  WHICH_APPLIANCE=appliance_2
  SIBLING_APPLIANCE=appliance_1
else
  echo "ERROR: Can't figure out who I'm supposed to be..."
  exit 255
fi

eval MY_PORT1_IP=$"$WHICH_APPLIANCE"_port1_ip
eval MY_PORT2_IP=$"$WHICH_APPLIANCE"_port2_ip

eval SIBLING_HOST=$"$SIBLING_APPLIANCE"_hostname
eval SIBLING_PORT1_IP=$"$SIBLING_APPLIANCE"_port1_ip
eval SIBLING_PORT2_IP=$"$SIBLING_APPLIANCE"_port2_ip

EXIT=0

if [ "$net1_dhcp" -eq "0" ] && [ -z "$nameserver" -a -z "$MY_PORT1_IP" ]; then
  echo "Must specify Port 1 IP if not using DHCP and DNS is not available"
  EXIT=255
fi

if [ "$net1_dhcp" -eq "0" ] && [ -z "$net1_netmask" ]; then
  echo "Must specify netmask if not using DHCP"
  EXIT=255
fi

if [ -z "$net1_gateway" ]; then
  echo "WARNING: no default route.  Addresses not on the LAN will be inaccessible"
fi

if [ "$net2_enabled" -eq "1" ] && [ -z "$MY_PORT2_IP" -o -z "$net2_netmask" ]; then
  echo "ERROR: Port 2 is enabled, but IP and netmask are not set"
  EXIT=255
fi

if [ -z "$nameserver" -a -z "$es_ip" ]; then
  echo "ERROR: ES IP must be set, since DNS is not available"
  EXIT=255
fi

if [ -z "$nameserver" -a \( -z "$appliance_1_port1_ip" -o -z "$appliance_2_port1_ip" \) ]; then
  echo "ERROR: Appliance IPs must be set, since DNS is not available"
  EXIT=255
fi

exit $EXIT

