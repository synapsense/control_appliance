#!/usr/bin/env sh

# This script crease a bootable USB key that will install an appliance
# firmware image automagically

USB_DEVICE=da0
BOOT_IMAGE_NAME=FreeBSD-10.1-RELEASE-amd64-uefi-mini-memstick.img.xz
APPLIANCE_IMAGE=appliance.img.xz

if [ ! -f $BOOT_IMAGE_NAME ]
then
	fetch ftp://ftp.freebsd.org/pub/FreeBSD/releases/amd64/amd64/ISO-IMAGES/10.1/$BOOT_IMAGE_NAME
fi

if [ ! -f $APPLIANCE_IMAGE ]
then
	fetch http://glados.synapsense.int/job/CI-SHADOWW-appliance/lastSuccessfulBuild/artifact/$APPLIANCE_IMAGE
fi

xzcat $BOOT_IMAGE_NAME | dd of=/dev/$USB_DEVICE bs=64k

gpart recover $USB_DEVICE
gpart delete -i 4 da0
gpart resize -i 3 -a 4k $USB_DEVICE
growfs -y /dev/${USB_DEVICE}p3

mount_dir=`env TMPDIR=. mktemp -d -t mount`
mkdir $mount_dir
mount /dev/${USB_DEVICE}p3 $mount_dir

echo "/dev/da0p3 / ufs ro,noatime 1 1" > $mount_dir/etc/fstab
cp rc.local $mount_dir/etc/rc.local
chown root:wheel $mount_dir/etc/rc.local
cp $APPLIANCE_IMAGE $mount_dir

umount $mount_dir
rmdir $mount_dir

