#!/bin/sh

# API for command-line or remote (SSH) control over the running
# SHADOWW node

[ `whoami` = root ] || exec sudo $0 $*

. /etc/appliance/appliance.subr

cmd=$1
shift

usage() {
  echo "Usage: $0 <command> [args]"
  echo ""
  echo "Commands:"
  echo "       profile <profile> - activate a SHADOWW profile for next restart"
  echo "                           Profile is 'clustered' or 'isolated'"
  echo ""
  echo "       start             - start the SHADOWW service.  Fails if not enabled"
  echo ""
  echo "       stop              - stop the SHADOWW service.  Fails if not enabled"
  echo ""
  echo "       save_passwd       - Save updated passwords to persistent storage"
  echo ""
  echo "       reboot            - Reboot the appliance"
  echo ""
  echo "       pause_es_sync     - Pause sync with ES until resumed or service restart"
  echo ""
  echo "       resume_es_sync    - Resume sync with ES until paused or service restart"
  echo ""
  echo "       pause_clustering  - Remove all other SHADOWW nodes from cluster and"
  echo "                           server connections.  Returns once all rooms are"
  echo "                           running on the current node"
  echo ""
  echo "       resume_clustering - Restore all other SHADOWW nodes to cluster and"
  echo "                           attempt connections.  Returns once the cluster"
  echo "                           is re-formed and configuration is re-distributed"
  echo ""
  echo "       cluster_stable    - Wait until cluster is formed and stabilized"
}

shadoww_ctrl="env HOME=/logs RUNNER_LOG_DIR=/logs ERL_CRASH_DUMP=/logs /shadoww/bin/shadoww escript bin/shadoww_ctrl"

case $cmd in
  profile)
    profile=$1
    case $profile in
      clustered)
	activate_clustered_profile
	;;
      isolated)
	activate_isolated_profile
	;;
      *)
	usage
	exit 1
	;;
    esac
    ;;
  start)
    $SHADOWW_RC start
    ;;
  stop)
    $SHADOWW_RC stop
    ;;
  save_passwd)
    save_passwd
    ;;
  reboot)
    reboot
    ;;
  pause_es_sync)
    $shadoww_ctrl pause_es_sync
    ;;
  resume_es_sync)
    $shadoww_ctrl resume_es_sync
    ;;
  pause_clustering)
    $shadoww_ctrl pause_clustering $*
    ;;
  resume_clustering)
    $shadoww_ctrl resume_clustering $*
    ;;
  cluster_stable)
    $shadoww_ctrl cluster_stable
    ;;
  attach)
    env HOME=/logs RUNNER_LOG_DIR=/logs ERL_CRASH_DUMP=/logs /shadoww/bin/shadoww attach
    ;;
  remote_console)
    env HOME=/logs RUNNER_LOG_DIR=/logs ERL_CRASH_DUMP=/logs /shadoww/bin/shadoww remote_console
    ;;
  *)
    usage
    exit 1
    ;;
esac

