#!/bin/sh

set -x

IMAGE=test.img
dd if=/dev/zero of=$IMAGE bs=4k count=500k
mdconfig -f $IMAGE -a -u 0

DEV=md0

CODESIZE=700m
CFGSIZE=100m

GP_ALIGN="-a 4k"
#GP_ALIGN=""

gpart create -s gpt $DEV
gpart add -t efi -s 800K $GP_ALIGN $DEV
gpart add -t freebsd-ufs -s $CODESIZE $GP_ALIGN $DEV
gpart add -t freebsd-ufs -s $CODESIZE $GP_ALIGN $DEV
gpart add -t freebsd-ufs -s $CFGSIZE $GP_ALIGN $DEV
gpart add -t freebsd-ufs $GP_ALIGN $DEV

read -p "press any key to continue" key

dd bs=4k if=/boot/boot1.efifat of=/dev/${DEV}p1

NF_ALIGN="-S 4096"
#NF_ALIGN=""
newfs -L code1 -n $NF_ALIGN /dev/${DEV}p2
newfs -L code2 -n $NF_ALIGN /dev/${DEV}p3
newfs -L cfg $NF_ALIGN /dev/${DEV}p4
newfs -L logs -U -t $NF_ALIGN /dev/${DEV}p5

read -p "press any key to continue" key

CODE_MNT=/home/gsmith/nanobsd/code
mkdir $CODE_MNT
mount /dev/${DEV}p2 $CODE_MNT
(cd  /usr/src; make KERNCONF=SHADOWW DESTDIR=$CODE_MNT installkernel)
(cd  /usr/src; make DESTDIR=$CODE_MNT installworld)
(cd  /usr/src; make DESTDIR=$CODE_MNT distribution)
umount $CODE_MNT

#dd bs=64k if=/dev/${DEV}p2 of=code1.img

mdconfig -d -u 0

